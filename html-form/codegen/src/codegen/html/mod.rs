use crate::parsing::validation::ValidatedField;
use proc_macro2::{Span, TokenStream};
use quote::quote_spanned;
use std::iter::repeat;
use syn::{spanned::Spanned, Ident};

/// Generate the abstract form representation, in a box for dynamic polymorphism to work
fn boxed_field(field: &ValidatedField) -> TokenStream {
    let ValidatedField {
        name,
        ty,
        attributes,
        validators,
        ..
    } = field;
    let ident = name.to_string();
    let span = ty.span();

    quote_spanned! {span => {
        Box::new({
            let mut field = <#ty>::form_field(#ident.to_string(), #attributes);
            field.add_validators(&mut #validators);
            field
        })
    }}
}

/// Add a field to the form
fn add_field((field, form_name): (&ValidatedField, &Ident)) -> TokenStream {
    let span = field.ty.span();
    let field = boxed_field(field);

    quote_spanned! {span => #form_name.add_field(#field);}
}

/// Generate a function to create a form, append all the fields
pub fn generate_form(fields: &[ValidatedField], crate_: &Ident) -> TokenStream {
    let form_name = Ident::new("form", Span::call_site());
    let form = fields.iter().zip(repeat(&form_name)).map(add_field);

    quote! {
        use ::#crate_::{HtmlAttributes, forms::{Form, IntoFormField, FormField, Input}};

        let mut #form_name = Form::new();
        #(#form)*

        #form_name
    }
}
