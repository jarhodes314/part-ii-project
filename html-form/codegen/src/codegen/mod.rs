//! The code generation modules
//!
//! These bits actually output the code that is used
pub mod html;
pub mod validation;
