use crate::parsing::validation::{double_entry::DoubleEntry, ValidatedField};
use proc_macro2::TokenStream;
use syn::{Generics, Ident};

fn collect_validators(
    fields: &[&ValidatedField],
    struct_name: &Ident,
    generics: &Generics,
) -> TokenStream {
    let field_names = fields.iter().map(|field| &field.name);
    let field_types = fields.iter().map(|field| &field.ty);

    quote! {
        struct #struct_name #generics {
            #(#field_names: Vec<Box<dyn ValidationRule<#field_types>>>),*
        }
    }
}

fn generate_validators(fields: &[&ValidatedField], struct_name: &Ident) -> TokenStream {
    let field_names = fields.iter().map(|field| &field.name);
    let field_validators = fields.iter().map(|field| &field.validators);

    quote! {
        #struct_name {
            #(#field_names: #field_validators),*
        }
    }
}

pub fn generate_backend(
    fields: &[ValidatedField],
    struct_name: &Ident,
    generics: &Generics,
    crate_: &Ident,
) -> TokenStream {
    let struct_name = quote::format_ident!("{}Validator", struct_name);
    let fields = fields
        .iter()
        .filter(|field| field.double_entry != DoubleEntry::Confirm)
        .collect::<Vec<_>>();
    let validator_struct = collect_validators(&fields, &struct_name, generics);
    let validators = generate_validators(&fields, &struct_name);
    let field_names = fields.iter().map(|field| &field.name);

    quote! {
        use ::#crate_::validation::ValidationRule;
        #validator_struct;
        let validators = #validators;

        let mut errors = Vec::new();
        #(errors.append(&mut validators.#field_names.iter().filter_map(|v| v.validate(&self.#field_names).err()).collect());)*

        if errors.len() > 0 {
            Err(errors)
        } else {
            Ok(())
        }
    }
}

pub fn generate_from_form(
    fields: &[ValidatedField],
    struct_name: &Ident,
    crate_: &Ident,
) -> TokenStream {
    let field_name_no_confirm: Vec<_> = fields
        .iter()
        .filter(|field| field.double_entry != DoubleEntry::Confirm)
        .map(|field| &field.name)
        .collect();
    let field_name_all: Vec<_> = fields.iter().map(|field| field.name.to_string()).collect();
    let field_name_tmp_all: Vec<_> = fields
        .iter()
        .map(|field| quote::format_ident!("{}_tmp", field.name))
        .collect();
    let field_name_tmp_no_confirm: Vec<_> = fields
        .iter()
        .filter(|field| field.double_entry != DoubleEntry::Confirm)
        .map(|field| quote::format_ident!("{}_tmp", field.name))
        .collect();
    let field_ty = fields.iter().map(|field| &field.ty);
    let (double_entry, double_entry_name): (Vec<_>, Vec<_>) = fields
        .iter()
        .filter(|field| field.double_entry == DoubleEntry::Yes)
        .map(|field| (&field.name, field.name.to_string()))
        .map(|(a, b)| (quote::format_ident!("{}_tmp", a), b))
        .unzip();
    let double_entry_confirm: Vec<_> = fields
        .iter()
        .filter(|field| field.double_entry == DoubleEntry::Confirm)
        .map(|field| quote::format_ident!("{}_tmp", field.name))
        .collect();

    quote! {
        use ::#crate_::validation::BackendValidated;
        use ::rocket::{http::RawStr, request::FromFormValue};

        let empty = RawStr::from_str("");
        #(let mut #field_name_tmp_all = empty;)*

        for item in items {
            match item.key.as_str() {
                #(#field_name_all =>
                    #field_name_tmp_all = item.value,
                )*
                _ => (),
            };
        }

        #(let #field_name_tmp_all = <#field_ty>::from_form_value(#field_name_tmp_all)
              .or(Err(vec![
                      Box::new(format!("could not parse {}", #field_name_all)) as Box<dyn std::fmt::Debug>
              ]))?;)*

        let mut double_entry_errors = Vec::new();

        #(if #double_entry != #double_entry_confirm {
            double_entry_errors.push(Box::new(format!("{}s do not match", #double_entry_name)) as Box<dyn std::fmt::Debug>);
        })*

        let result = #struct_name {
            // Omit the confirm fields for double entry from the result struct
            #(#field_name_no_confirm: #field_name_tmp_no_confirm),*
        };

        let mut validated = result.validate_form_values();

        if double_entry_errors.len() > 0 {
            match &mut validated {
                Ok(_) => validated = Err(double_entry_errors),
                Err(ref mut errors) => errors.append(&mut double_entry_errors),
            };
        }

        validated.and(Ok(result))
    }
}
