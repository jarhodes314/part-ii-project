//! # `HTMLForm` Macro
//!
//! This is the crate containing the custom derive powering `HtmlForm`, a way of generating HTML
//! forms automatically for the [Rocket web framework](https://rocket.rs)

#![warn(
    clippy::all,
    clippy::restriction,
    clippy::pedantic,
    clippy::nursery,
//    clippy::cargo
)]
#![allow(
    clippy::restriction::implicit_return,
    clippy::restriction::missing_inline_in_public_items,
    clippy::restriction::wildcard_enum_match_arm,
    clippy::pedantic::single_match_else,
    clippy::pedantic::filter_map,
    clippy::restriction::integer_arithmetic
)]
#![cfg_attr(feature = "strict", deny(missing_docs))]
//#![cfg_attr(feature = "strict", deny(warnings))]
#![feature(proc_macro_diagnostic)]
#[macro_use]
extern crate syn;
#[macro_use]
extern crate proc_macro_error;
extern crate proc_macro;

use quote::quote;
use syn::{parse_quote, Data};

mod codegen;
mod parsing;
mod symbol;
use symbol::*;

/// A custom derive macro for HtmlForm. The single entry point for the entire library.
#[proc_macro_derive(
    HtmlForm,
    attributes(form_validation, html_form, html_form_debug, double_entry)
)]
#[proc_macro_error]
pub fn derive_html_form(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast = parse_macro_input!(input as syn::DeriveInput);
    let Attributes { crate_ } = Attributes::from_ast(&ast);
    let crate_ = crate_.unwrap_or_else(|| parse_quote! { html_form });

    let fields = if let Data::Struct(data) = ast.data {
        match parsing::input_struct(data, &crate_) {
            Ok(fields) => fields,
            Err(e) => abort!(e),
        }
    } else {
        abort_call_site!("This macro can only be applied to structs")
    };

    let ident = ast.ident;
    let generics = ast.generics;

    let html_form_impl = r#impl::html_form(&fields, &crate_, &ident, &generics);
    let backend_validated_impl = r#impl::backend_validated(&fields, &crate_, &ident, &generics);
    let from_form_impl = r#impl::from_form(&fields, &crate_, &ident, &generics);

    let output = quote! {
        #html_form_impl
        #backend_validated_impl
        #from_form_impl
    };

    proc_macro::TokenStream::from(output)
}

#[derive(Default)]
struct Attributes {
    crate_: Option<syn::Ident>,
}

use std::ops::Add;

impl Add for &Attributes {
    type Output = Attributes;

    fn add(self, other: Self) -> Self::Output {
        Attributes {
            crate_: self
                .crate_
                .as_ref()
                .or(other.crate_.as_ref())
                .map(ToOwned::to_owned),
        }
    }
}

impl std::iter::Sum for Attributes {
    fn sum<I: Iterator<Item = Attributes>>(iter: I) -> Self {
        let init = Attributes { crate_: None };
        iter.fold(init, |a0, a1| &a0 + &a1)
    }
}

fn get_meta_items(attr: &syn::Attribute, name: Symbol) -> Vec<syn::NestedMeta> {
    use syn::Meta;
    if attr.path != name {
        return Vec::new();
    }

    match attr.parse_meta() {
        Ok(Meta::List(m)) => m.nested.into_iter().collect(),
        _ => Vec::new(),
    }
}

impl Attributes {
    fn from_ast(item: &syn::DeriveInput) -> Self {
        use syn::{Meta, NestedMeta};
        let mut crate_ = None;

        for meta_item in item
            .attrs
            .iter()
            .flat_map(|attr| get_meta_items(attr, HTML_FORM))
        {
            match meta_item {
                NestedMeta::Meta(Meta::NameValue(nv)) if nv.path == CRATE => {
                    if let syn::Lit::Str(ref lit) = nv.lit {
                        crate_ = lit.parse().unwrap();
                    }
                }
                _ => (),
            }
        }

        Attributes { crate_ }
    }
}

mod r#impl {
    use crate::codegen::html::generate_form;
    use crate::codegen::validation;
    use crate::parsing::validation::ValidatedField;
    use proc_macro2::TokenStream;
    use syn::{Generics, Ident, Path};

    pub fn html_form(
        fields: &[ValidatedField],
        crate_: &Ident,
        ident: &Ident,
        generics: &Generics,
    ) -> TokenStream {
        let html_function = generate_form(fields, crate_);
        let (_, ty_generics, where_clause) = generics.split_for_impl();
        let lifetimes = generics.lifetimes();
        let type_params = generics.type_params();

        quote! {
            #[automatically_derived]
            impl<'f, #(#lifetimes,)* #(#type_params),*> ::#crate_::HtmlForm<'f> for #ident #ty_generics #where_clause {
                fn generate_form() -> ::#crate_::forms::Form<'f> {
                    #html_function
                }
            }
        }
    }

    pub fn backend_validated(
        fields: &[ValidatedField],
        crate_: &Ident,
        ident: &Ident,
        generics: &Generics,
    ) -> TokenStream {
        let backend_validation = validation::generate_backend(fields, ident, generics, crate_);
        let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
        let error_trait: Path = syn::parse_quote! { ::std::fmt::Debug };

        quote! {
            #[automatically_derived]
            impl #impl_generics ::#crate_::validation::BackendValidated for #ident #ty_generics #where_clause {
                fn validate_form_values(&self) -> Result<(), Vec<Box<dyn #error_trait>>> {
                    #backend_validation
                }
            }
        }
    }

    pub fn from_form(
        fields: &[ValidatedField],
        crate_: &Ident,
        ident: &Ident,
        generics: &Generics,
    ) -> TokenStream {
        let fromform_function = validation::generate_from_form(fields, ident, crate_);
        let (_, ty_generics, where_clause) = generics.split_for_impl();
        let lifetimes = generics.lifetimes();
        let type_params = generics.type_params();

        quote! {
            #[automatically_derived]
            impl <'f, #(#lifetimes,)* #(#type_params),*> ::rocket::request::FromForm<'f> for #ident #ty_generics #where_clause {
                type Error = Vec<Box<dyn ::std::fmt::Debug>>;

                fn from_form(items: &mut ::rocket::request::FormItems<'f>, strict: bool) -> Result<#ident #ty_generics, Self::Error> {
                    #fromform_function
                }
            }
        }
    }
}

/// Custom derive for SelectOne, applied to an enum for radio buttons/select
#[proc_macro_derive(SelectOne)]
#[proc_macro_error]
pub fn derive_select_one(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    unimplemented!()
}

/// Custom derive for SelectMany, applied to an enum for e.g. checkboxes
#[proc_macro_derive(SelectMany)]
#[proc_macro_error]
pub fn derive_select_many(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    unimplemented!()
}
