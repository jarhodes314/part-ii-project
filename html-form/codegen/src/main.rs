use codegen::HtmlForm;
use util::{validation::MinimumLength, HtmlForm, InRange};

#[derive(HtmlForm)]
#[html_form(crate = "util")]
struct Test {
    #[form_validation(MinimumLength(6))]
    #[html_form(label = "Hello", placeholder = "Something")]
    #[double_entry]
    test: String,
    #[form_validation(InRange![1..5])]
    bounded_number: i32,
}

fn main() {
    println!("{:#?}", Test::generate_form().html())
}
