use crate::symbol::HTML_FORM_DEBUG;
use lazy_static::lazy_static;
use std::sync::RwLock;
use syn::parse::{Parse, ParseStream, Result as ParseResult};
use syn::Ident;

/// Whether debug-mode should be enabled for validation or html parsing - print out the parsed
/// output if so
#[derive(Debug, Clone)]
struct DebugEnable {
    validation: bool,
    html_config: bool,
}

impl DebugEnable {
    /// Creates a DebugEnable with the default values of `false` for both
    const fn new() -> Self {
        Self {
            validation: false,
            html_config: false,
        }
    }

    /// Combines two [DebugEnable]s by ORing the values of the fields, propagating parse errors
    fn reduce(acc: ParseResult<Self>, new: ParseResult<Self>) -> ParseResult<Self> {
        let acc = acc?;
        let new = new?;
        Ok(Self {
            validation: acc.validation || new.validation,
            html_config: acc.html_config || new.html_config,
        })
    }
}

impl Parse for DebugEnable {
    fn parse(input: ParseStream) -> ParseResult<Self> {
        let mut result = Self::new();
        let content;

        parenthesized!(content in input);
        let values = content.parse_terminated::<Ident, Token![,]>(Ident::parse)?;

        for ident in values {
            match ident.to_string().as_str() {
                "validation" => result.validation = true,
                "html_config" => result.html_config = true,
                _ => return Err(syn::Error::new(ident.span(), "unknown debug mode")),
            };
        }

        Ok(result)
    }
}

lazy_static! {
    static ref DEBUG_ENABLE: RwLock<Option<ParseResult<DebugEnable>>> = RwLock::new(None);
}

fn parse_debug_enable(attrs: &[syn::Attribute]) -> ParseResult<DebugEnable> {
    attrs
        .iter()
        .filter(|attr| attr.path == HTML_FORM_DEBUG)
        .map(|attr| syn::parse(attr.tokens.clone().into()))
        .fold(Ok(DebugEnable::new()), DebugEnable::reduce)
}

unsafe fn debug_enable(attrs: &[syn::Attribute]) -> ParseResult<DebugEnable> {
    // Check whether there is a cached value
    if DEBUG_ENABLE.read().unwrap().is_none() {
        // If there isn't, parse and cache
        let mut debug_enable = DEBUG_ENABLE.write().unwrap();
        *debug_enable = Some(parse_debug_enable(attrs));
    }

    DEBUG_ENABLE.read().unwrap().as_ref().unwrap().to_owned()
}

pub fn display_validation_parse(attrs: &[syn::Attribute]) -> ParseResult<bool> {
    unsafe { debug_enable(attrs).map(|d| d.validation) }
}

pub fn display_html_parse(attrs: &[syn::Attribute]) -> ParseResult<bool> {
    unsafe { debug_enable(attrs).map(|d| d.html_config) }
}
