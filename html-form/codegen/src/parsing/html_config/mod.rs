use super::util::display_html_parse as is_debug;
use crate::symbol::*;
use syn::Attribute;
pub use util::HtmlAttributes;

pub fn parse_attributes(attrs: &[Attribute]) -> syn::parse::Result<HtmlAttributes> {
    let html_attributes = attrs
        .iter()
        .filter(|attr| attr.path == HTML_FORM)
        .map(|attr| syn::parse(attr.tokens.clone().into()))
        .fold(Ok(HtmlAttributes::new()), HtmlAttributes::reduce)?;

    if is_debug(attrs)? {
        println!("{:#?}", html_attributes);
    }

    Ok(html_attributes)
}
