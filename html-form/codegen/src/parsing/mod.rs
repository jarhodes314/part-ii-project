//! A collection of all the parsing related stuff
mod debug;
mod html_config;
pub mod validation;
mod util {
    pub use super::debug::*;
    pub use ::util::*;
}

use syn::{spanned::Spanned, DataStruct, Ident};
use validation::{
    double_entry::{confirmation_field, DoubleEntry},
    ValidatedField,
};

pub fn input_struct(data: DataStruct, crate_: &Ident) -> syn::parse::Result<Vec<ValidatedField>> {
    if let syn::Fields::Named(syn::FieldsNamed { named: fields, .. }) = data.fields {
        let mut validated_fields = Vec::new();

        for field in fields {
            add_validators(&mut validated_fields, field, crate_)?;
        }

        Ok(validated_fields)
    } else {
        abort_call_site!("This macro can only be used with named fields")
    }
}

/// Parse a field and append it to the list of fields already parsed
fn add_validators(
    validated_fields: &mut Vec<ValidatedField>,
    field: syn::Field,
    crate_: &Ident,
) -> syn::parse::Result<()> {
    let span = field.span();
    let expected_named_fields = || syn::Error::new(span, "expected named fields");
    let name = field.ident.ok_or_else(expected_named_fields)?;
    let colon = field.colon_token.ok_or_else(expected_named_fields)?;
    let ty = field.ty;
    let validators = validation::parse_validators(&field.attrs)?;
    let attributes = html_config::parse_attributes(&field.attrs)?;
    let double_entry = validation::double_entry::parse_double_entry(&field.attrs);

    let field = ValidatedField {
        name,
        colon,
        ty,
        validators,
        attributes,
        double_entry,
    };

    if double_entry == DoubleEntry::Yes {
        validated_fields.push(confirmation_field(&field, crate_));
    }

    validated_fields.push(field);

    Ok(())
}
