//! Double entry specific validation functions
//!
//! Because double entry is a bit magic and adds a field to the form that doesn't exist in the
//! original struct, it is an edge case for the macro and isn't implemented in the same way as
//! other validators. It also requires the value of another field to perform the backend validation
//! so it just wouldn't work.

use super::ValidatedField;
use crate::symbol::DOUBLE_ENTRY;
use syn::Ident;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DoubleEntry {
    /// Field isn't entered twice
    No,
    /// Field is entered twice
    Yes,
    /// Confirmation field
    Confirm,
}

/// Create a confirmation field
pub fn confirmation_field(field: &ValidatedField, crate_: &Ident) -> ValidatedField {
    let prepend_confirm = |label| format!("Confirm {}", label);
    let mut field = field.clone();
    let old_name = field.name.to_string();
    let old_label = match field.attributes.label {
        Some(ref l) => quote! { Some(#l) },
        None => quote! { None },
    };
    field.name = quote::format_ident!("confirm_{}", field.name);
    field.attributes.label = field.attributes.label.map(prepend_confirm);
    field
        .validators
        .0
        .push(parse_quote! {#crate_::validation::DoubleEntry{ident: #old_name, label: #old_label}});
    field.double_entry = DoubleEntry::Confirm;

    field
}

pub fn parse_double_entry(attrs: &[syn::Attribute]) -> DoubleEntry {
    let double_entry_count = attrs
        .iter()
        .filter(|attr| attr.path == DOUBLE_ENTRY)
        .count();
    match double_entry_count {
        0 => DoubleEntry::No,
        _ => DoubleEntry::Yes,
    }
}
