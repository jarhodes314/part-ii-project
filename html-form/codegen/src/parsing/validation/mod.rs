//! The validation parsing
use std::convert::TryFrom;
use syn::{
    parse::Result as ParseResult,
    parse::{Parse, ParseStream},
    punctuated::Punctuated,
    spanned::Spanned,
    Attribute, Ident,
};

use super::{html_config::HtmlAttributes, util::display_validation_parse as is_debug};
use crate::symbol::*;
use proc_macro2::{Delimiter, Group, Punct, Spacing, Span, TokenStream};
use quote::{ToTokens, TokenStreamExt};

pub mod double_entry;

#[derive(Clone, Debug)]
pub struct ValidatedField {
    pub name: Ident,
    pub colon: Token![:],
    pub ty: syn::Type,
    pub validators: Validators,
    pub attributes: HtmlAttributes,
    pub double_entry: double_entry::DoubleEntry,
}

#[derive(Clone, Debug)]
pub struct Validators(pub Vec<ValidatorExpr>);

#[derive(Clone, Debug)]
pub enum ValidatorExpr {
    Call(syn::ExprCall),
    MethodCall(syn::ExprMethodCall),
    Struct(syn::ExprStruct),
    Macro(syn::ExprMacro),
}

impl Parse for ValidatorExpr {
    fn parse(stream: ParseStream) -> ParseResult<Self> {
        use syn::Expr;
        use ValidatorExpr::*;

        match stream.parse()? {
            Expr::Call(c) => Ok(Call(c)),
            Expr::MethodCall(m) => Ok(MethodCall(m)),
            Expr::Struct(s) => Ok(Struct(s)),
            Expr::Macro(m) => Ok(Macro(m)),
            e => Err(syn::Error::new(
                e.span(),
                "expected method call or struct literal",
            )),
        }
    }
}

impl ToTokens for ValidatorExpr {
    fn to_tokens(&self, ts: &mut TokenStream) {
        use ValidatorExpr::*;

        match self {
            Call(c) => c.to_tokens(ts),
            MethodCall(m) => m.to_tokens(ts),
            Struct(s) => s.to_tokens(ts),
            Macro(m) => m.to_tokens(ts),
        };
    }
}

impl ToTokens for Validators {
    fn to_tokens(&self, ts: &mut TokenStream) {
        ts.append(Ident::new("vec", Span::call_site()));
        ts.append(Punct::new('!', Spacing::Alone));
        let validator = self.0.iter();
        ts.append(Group::new(
            Delimiter::Bracket,
            quote! { #(Box::new(#validator)),* },
        ));
    }
}

impl TryFrom<&[Attribute]> for Validators {
    type Error = syn::Error;

    fn try_from(attrs: &[Attribute]) -> ParseResult<Self> {
        type AttributeParse<T> = ParseResult<Vec<Punctuated<T, Token![,]>>>;

        Ok(Self(
            attrs
                .iter()
                .filter(|attr| attr.path == FORM_VALIDATION)
                .map(|attr| attr.parse_args_with(Punctuated::parse_terminated))
                .collect::<AttributeParse<ValidatorExpr>>()?
                .into_iter()
                .flat_map(Punctuated::into_iter)
                .collect::<Vec<_>>(),
        ))
    }
}

/// Parse the validators for a single field from the macro attributes
pub fn parse_validators(attrs: &[Attribute]) -> ParseResult<Validators> {
    let result = Validators::try_from(attrs)?;

    if is_debug(attrs)? {
        println!("{:#?}", result);
    }

    Ok(result)
}
