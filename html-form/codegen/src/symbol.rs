// Symbol code copied from https://github.com/serde-rs/serde/blob/3ea85a28cfbfbcad764e28cdedb34d27ea3bd926/serde_derive/src/internals/symbol.rs

use std::fmt::{self, Display};
use syn::{Ident, Path};

#[derive(Copy, Clone)]
pub struct Symbol(&'static str);

impl PartialEq<Symbol> for Ident {
    fn eq(&self, word: &Symbol) -> bool {
        self == word.0
    }
}

impl<'a> PartialEq<Symbol> for &'a Ident {
    fn eq(&self, word: &Symbol) -> bool {
        *self == word.0
    }
}

impl PartialEq<Symbol> for Path {
    fn eq(&self, word: &Symbol) -> bool {
        self.is_ident(word.0)
    }
}

impl<'a> PartialEq<Symbol> for &'a Path {
    fn eq(&self, word: &Symbol) -> bool {
        self.is_ident(word.0)
    }
}

impl Display for Symbol {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(self.0)
    }
}

pub const CRATE: Symbol = Symbol("crate");
pub const DOUBLE_ENTRY: Symbol = Symbol("double_entry");
pub const FORM_VALIDATION: Symbol = Symbol("form_validation");
pub const HTML_FORM: Symbol = Symbol("html_form");
pub const HTML_FORM_DEBUG: Symbol = Symbol("html_form_debug");
