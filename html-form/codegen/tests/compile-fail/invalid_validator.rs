extern crate html_form;
use html_form::HtmlForm;

#[derive(HtmlForm)]
struct Test {
    #[form_validation(min_length=6)] //~ ERROR expected method call or struct literal
    test: String,
}

fn main() {}
