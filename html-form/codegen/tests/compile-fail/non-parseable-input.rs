extern crate html_form;
extern crate rocket;
use html_form::HtmlForm;

#[derive(HtmlForm)]
struct Test {
    #[form_validation(
        UnknownValidator(3) //~ ERROR cannot find function, tuple struct or tuple variant `UnknownValidator` in this scope [E0425]
    )]
    test: String,
}

fn main() {}
