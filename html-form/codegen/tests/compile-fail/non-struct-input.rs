extern crate html_form;
use html_form::HtmlForm;

#[derive(HtmlForm)] //~ ERROR This macro can only be applied to structs
enum MyOption<T> {
    Some(T),
    None,
}

fn main() {}
