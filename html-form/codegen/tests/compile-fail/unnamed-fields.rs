extern crate html_form;
use html_form::HtmlForm;

#[derive(HtmlForm)] //~ ERROR This macro can only be used with named fields
struct UnnamedFields(String, i32);

fn main() {}
