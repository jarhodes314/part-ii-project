//! Tests for the compilation of various macro-y code
//!
//! This is a file that will invoke `compiletest` as part of `cargo test`, wihch will check that
//! e.g. all the files in `compile-fail` fail to compile with the specified error in the specified
//! place.
extern crate compiletest_rs as compiletest;

use std::path::PathBuf;

fn run_mode(mode: &'static str, custom_dir: Option<&'static str>) {
    let mut config = compiletest::Config::default().tempdir();
    let cfg_mode = mode.parse().expect("Invalid mode");

    config.mode = cfg_mode;

    let dir = custom_dir.unwrap_or(mode);
    config.src_base = PathBuf::from(format!("tests/{}", dir));
    config.target_rustcflags = Some("-L ../target/debug -L ../target/debug/deps".to_string());

    // Set the build directory to a clean one (not usually necessary)
    //config.clean_rmeta();

    if config.src_base.exists() {
        compiletest::run_tests(&config);
    }
}

#[test]
fn compile_test() {
    run_mode("compile-fail", None);
    run_mode("run-pass", None);
    run_mode("ui", None);
}
