// Test to check the parsing of simple validation rules succeeds
// build-pass

extern crate html_form;
extern crate rocket;
use html_form::HtmlForm;

#[derive(HtmlForm)]
struct Something {
    #[html_form(label = "A string", value = "Hello")]
    #[html_form_debug(html_config)]
    a_string: String,
}

pub fn main() {}
