// Test to check the parsing of simple validation rules succeeds
// Also checks the custom crate macro

// build-pass

extern crate html_form;
extern crate rocket;

use html_form::{HtmlForm, validation::{MinimumLength, MaximumSymbols}};

#[derive(HtmlForm)]
struct Something {
    #[form_validation(MinimumLength(6))]
    #[form_validation(MaximumSymbols(3), MinimumLength(10))]
    #[html_form_debug(validation)]
    a_string: String,
}

pub fn main() {}
