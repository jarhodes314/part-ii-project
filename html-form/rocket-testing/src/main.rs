// Enable some required crate features for Rocket
#![feature(decl_macro, proc_macro_hygiene)]

//! # HTMLForm Testing
//!
//! This is the website, created using Rocket, that is used to test the parts of the util crate in
//! `html_form` that can't be tested without an application (e.g. template filters)

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde;

use html_form::{Html, HtmlForm};
use indexmap::IndexMap;
use rocket::request::Form;
use rocket::{Rocket, Route};
use rocket_contrib::templates::Template;
#[cfg(test)]
mod tests;

fn routes() -> Vec<Route> {
    routes![
        simple_html,
        test_form,
        display_test_form,
        example_double_entry,
        example_number_entry,
        test_double_entry,
        test_number_entry,
        example,
        login,
    ]
}

#[derive(Serialize)]
struct SimpleHtml {
    content: Html,
}

#[get("/simple-html")]
fn simple_html() -> Template {
    use html_form::{Content, Html, Tag, TagVariety::*};

    let mut nested_text = Html::new();
    nested_text.push_back(Content::Raw(String::from("MEOW")));

    let mut nested = Html::new();
    nested.push_back(Content::Tag(Nest(Tag {
        name: String::from("b"),
        attributes: IndexMap::new(),
        nested: Some(nested_text),
    })));
    nested.push_back(Content::Raw(String::from(" prrrrr")));

    let mut content = Html::new();
    content.push_back(Content::Tag(Nest(Tag {
        name: String::from("p"),
        attributes: IndexMap::new(),
        nested: Some(nested),
    })));

    Template::render("htmlify", SimpleHtml { content })
}

#[derive(HtmlForm)]
struct SimpleValidation {
    #[form_validation(html_form::validation::MinimumLength(4))]
    #[html_form(label = "A label")]
    test: String,
}

#[derive(HtmlForm)]
struct SomeData {
    #[html_form(placeholder = "Name")]
    name: String,
    #[form_validation(
        html_form::InRange![18..])]
    age: i32,
    optional: String,
    #[form_validation(html_form::validation::MinimumLength(8))]
    #[double_entry]
    password: String,
}

#[derive(HtmlForm)]
struct DoubleEntry {
    #[double_entry]
    #[form_validation(html_form::validation::MinimumLength(8))]
    #[html_form(label = "Password")]
    test: String,
}

#[derive(HtmlForm)]
struct NumberForm {
    #[form_validation(html_form::InRange![1..5])]
    #[html_form(label = "A number")]
    random_number: i32,
}

#[post("/test-form", data = "<form>")]
fn test_form(form: Form<SimpleValidation>) -> String {
    form.into_inner().test
}

#[get("/test-form")]
fn display_test_form() -> Template {
    #[derive(Serialize)]
    struct Context {
        form: html_form::forms::SerializeForm,
    }

    Template::render(
        "form_structure",
        Context {
            form: SimpleValidation::generate_form().into(),
        },
    )
}

#[post("/double-entry", data = "<form>")]
fn test_double_entry(form: Form<DoubleEntry>) -> String {
    form.into_inner().test
}

#[get("/double-entry")]
fn example_double_entry() -> Template {
    #[derive(Serialize)]
    struct Context {
        form: html_form::forms::SerializeForm,
    }

    Template::render(
        "validated_form",
        Context {
            form: DoubleEntry::generate_form().into(),
        },
    )
}

#[post("/number-form", data = "<form>")]
fn test_number_entry(form: Form<NumberForm>) -> String {
    form.into_inner().random_number.to_string()
}

#[get("/number-form")]
fn example_number_entry() -> Template {
    #[derive(Serialize)]
    struct Context {
        form: html_form::forms::SerializeForm,
    }

    Template::render(
        "validated_form",
        Context {
            form: NumberForm::generate_form().into(),
        },
    )
}

#[get("/some-data")]
fn example() -> Template {
    #[derive(Serialize)]
    struct Context {
        form: html_form::forms::SerializeForm,
    }

    Template::render(
        "validated_form",
        Context {
            form: SomeData::generate_form().into(),
        },
    )
}

use bcrypt::DEFAULT_COST;
use html_form::forms::fields::BcryptPassword;
use rocket_contrib::json::Json;

#[derive(HtmlForm)]
struct Login {
    username: String,
    password: BcryptPassword,
}

#[derive(Serialize)]
struct PasswordVerification {
    hash: String,
    verified: bool,
}

#[post("/login", data = "<form>")]
fn login(form: Form<Login>) -> Json<PasswordVerification> {
    let hash = form.password.hash(DEFAULT_COST).unwrap();
    Json(PasswordVerification {
        verified: form.password.verify(&hash).unwrap(),
        hash,
    })
}

fn rocket() -> Rocket {
    rocket::ignite()
        .attach(Template::custom(|engines| {
            engines.tera.register_filter("htmlify", |value, args| {
                html_form::form_filter(&value, &args)
            });
            engines.tera.register_filter("validate", |value, args| {
                html_form::forms::validate_field(&value, &args)
            });
        }))
        .mount("/", routes())
}

fn main() {
    rocket().launch();
}
