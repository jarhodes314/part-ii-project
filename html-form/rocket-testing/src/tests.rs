use super::rocket;
use rocket::http::{ContentType, Status};
use rocket::local::Client;

#[test]
fn html_filter_does_something() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::simple_html);
    let mut response = client.get(route.path()).dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(
        response.body_string(),
        Some("<p ><b >MEOW</b> prrrrr</p>\n".into())
    );
}

#[test]
fn form_rejects_if_validation_fails() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_form);
    let response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("test=abc")
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn form_accepts_if_validation_succeeds() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_form);
    let mut response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("test=abcde")
        .dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body_string(), Some("abcde".into()));
}

#[test]
fn form_can_be_placed_on_template() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::display_test_form);
    let mut response = client.get(route.path()).dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(
        response.body_string(),
        Some("<form ><label name=\"test\">A label<input minlength=\"4\" name=\"test\" type=\"text\"></label></form>\n<form method=post action=\"\">\nA label -- <input minlength=\"4\" name=\"test\" type=\"text\">\n\n<input type=submit text=Submit>\n</form>\n".into())
    );
}

#[test]
fn form_rejects_if_double_data_entry_fails() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_double_entry);
    let response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("test=abcdefgh&confirm_test=abcdeshe")
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn form_accepts_if_double_data_entry_succeeds() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_double_entry);
    let mut response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("test=abcdefgh&confirm_test=abcdefgh")
        .dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body_string(), Some("abcdefgh".into()));
}

#[test]
fn form_rejects_if_number_out_of_range_above() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_number_entry);
    let response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("random_number=22")
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn form_rejects_if_number_out_of_range_below() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_number_entry);
    let response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("random_number=0")
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn form_accepts_if_number_in_range() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_number_entry);
    let mut response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("random_number=2")
        .dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body_string(), Some("2".into()));
}

#[test]
fn form_rejects_if_float_not_integer() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_number_entry);
    let response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("random_number=2.4")
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn form_rejects_if_integer_wrong_type() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::test_number_entry);
    let response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("random_number=hello")
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn login_form_verifies_password_correctly() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::login);
    let mut response = client
        .post(route.path())
        .header(ContentType::Form)
        .body("username=hello&password=world")
        .dispatch();

    #[derive(Deserialize)]
    struct Password {
        verified: bool,
    }

    assert_eq!(response.status(), Status::Ok);
    if let Ok(password) =
        serde_json::from_str(&response.body_string().unwrap()) as serde_json::Result<Password>
    {
        assert!(password.verified);
    } else {
        // Should return an object
        assert!(false)
    }
}
