//! # HTML Form
//!
//! This crate is designed to generate forms for the Rocket web framework.
#![cfg_attr(feature = "strict", deny(missing_docs))]
pub use codegen::HtmlForm;
//pub use util::{HtmlAttributes, HtmlForm, form_filter};
pub use util::*;
