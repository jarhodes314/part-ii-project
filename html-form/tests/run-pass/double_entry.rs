//! Check that the double entry fields are filtered out when collecting data so we don't get the
//! incorrect deserialisation, which here will result in a type error

extern crate html_form;
extern crate rocket;

use html_form::HtmlForm;

#[derive(HtmlForm)]
struct Somthing {
    #[double_entry]
    something: String,
    something_else: i32,
}

fn main() {}
