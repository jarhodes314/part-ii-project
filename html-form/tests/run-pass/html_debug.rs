extern crate html_form;
extern crate rocket;
extern crate indexmap;

use html_form::{HtmlForm, tag};
use html_form::{Html, Content, Tag, TagVariety};
use indexmap::indexmap;

#[derive(HtmlForm)]
struct Test {
    #[form_validation(html_form::validation::MinimumLength(6))]
    #[html_form(label = "Hello", placeholder = "Something")]
    field_name: String,
    a_number: i32,
}

fn comparison_0() -> Html {
    let mut nested = Html::new();
    nested.0.push_back(Content::Raw(String::from("Hello")));
    nested.0.push_back(tag!(NonNest, "input", [
        "name" => "field_name",
        "type" => "text",
        "placeholder" => "Something",
        "minlength" => "6"
    ]));

    let mut html = Html::new();
    html.0
        .push_back(tag!(Nest, "label", ["name" => "field_name"], Some(nested)));
    html
}

fn comparison_1() -> Html {
    let mut html = Html::new();
    let tag = tag!(NonNest, "input", [
        "name" => "a_number",
        "type" => "number",
        "step" => "1"
    ]);
    html.0.push_back(tag);
    html
}

fn main() {
    assert_eq!(comparison_0(), Test::generate_form().fields[0].html());
    assert_eq!(comparison_1(), Test::generate_form().fields[1].html());
}
