extern crate html_form;
extern crate rocket;
extern crate indexmap;

use html_form::{HtmlForm, tag, Html, Tag, TagVariety, validation::BackendValidated};
use indexmap::indexmap;

#[derive(HtmlForm)]
struct Test {
    #[form_validation(html_form::validation::MinimumLength(6))]
    #[html_form(label = "Hello", placeholder = "Something")]
    password: String,
}

fn main() {
    let too_short = Test { password: String::from("abcde") };
    let long_enough = Test { password: String::from("abcdef") };
    let also_long_enough = Test { password: String::from("abcdefgh") };
    assert!(too_short.validate_form_values().is_err());
    assert!(long_enough.validate_form_values().is_ok());
    assert!(also_long_enough.validate_form_values().is_ok());
}
