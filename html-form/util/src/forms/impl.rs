//! Implementations for some input objects
use super::{Field, Input};
use crate::html::{
    tags::{self, HtmlTag},
    Html,
};
use crate::HtmlAttributes;
use indexmap::IndexMap;
use serde::Serialize;
use std::fmt::Debug;

/// A label for an HTML form input
#[derive(Clone, Debug)]
pub struct Label {
    content: String,
}

impl Label {
    /// Creates a label
    pub fn new(content: String) -> Label {
        Label { content }
    }
}

impl From<&Label> for Html {
    fn from(label: &Label) -> Html {
        let label: Label = label.clone();
        Html::from(label)
    }
}

impl From<Label> for Html {
    fn from(label: Label) -> Html {
        let mut html = Html::new();
        html.push_back(Html::nestable_tag("label"));
        html.nest_back(Html::raw_text(label.content));
        html
    }
}

/// Represents an `<input type=?>`
#[derive(Debug, Serialize)]
pub struct InputTag {
    name: String,
    id: Option<String>,
    attributes: IndexMap<String, String>,
    value: Option<String>,
}

impl Input for InputTag {
    fn name(&self) -> &str {
        &self.name
    }

    fn id(&self) -> Option<&str> {
        None
    }

    fn html(&self) -> Html {
        let mut html = Html::new();
        html.push_back(tags::Input::html(&self.attributes, &self.value));
        html
    }

    fn attributes(&self) -> &IndexMap<String, String> {
        &self.attributes
    }
}

/// Links the Rust type to deserialize into to the representation used to build the form input
pub trait IntoInput: Sized {
    /// The result type for the method of this trait
    type Input;

    /// Creates the input
    fn html_input(
        id: Option<String>,
        name: String,
        attributes: IndexMap<String, String>,
        value: Option<String>,
    ) -> Self::Input;
}

pub trait IntoFormField {
    type Field;
    fn form_field(id: String, attributes: HtmlAttributes) -> Self::Field;
}

#[macro_export]
macro_rules! impl_into_input {
    ($ty:ty, $type: tt, [$($attr_name:tt = $attr_value:tt),*]) => {
        impl IntoInput for $ty {
            type Input = InputTag;

            fn html_input(
                id: Option<String>,
                name: String,
                mut attributes: IndexMap<String, String>,
                value: Option<String>,
            ) -> Self::Input {
                attributes.insert(String::from("type"), String::from($type));
                attributes.insert(String::from("name"), name.clone());
                $(attributes.insert(String::from($attr_name), String::from($attr_value));)*
                InputTag {
                    name,
                    id,
                    attributes,
                    value,
                }
            }
        }
    };

    ($ty: ty, $type:tt) => { impl_into_input!($ty, $type, []); };
}

pub struct Password(pub String);

impl_into_input!(String, "text");
impl_into_input!(Password, "password");
impl_into_input!(i32, "number", ["step" = "1"]);
impl_into_input!(f32, "number", ["step" = "0.01"]);
#[cfg(feature = "secure-password")]
impl_into_input!(super::secure_password::BcryptPassword, "password");

macro_rules! impl_into_form_field {
    ($ty:ty, [$($validators:expr),*]) => {
        impl IntoFormField for $ty {
            type Field = Field<InputTag>;

            fn form_field(name: String, attributes: HtmlAttributes) -> Self::Field {
                let mut attrs = IndexMap::new();
                attributes
                    .placeholder
                    .map(|placeholder| attrs.insert(String::from("placeholder"), placeholder));
                let input = Self::html_input(None, name.clone(), attrs, attributes.value);
                Field {
                    name,
                    label: attributes.label,
                    input,
                    validators: Vec::new(),
                }
            }
        }
    };

    ($ty:ty) => {
        impl_into_form_field!($ty, []);
    };
}

impl_into_form_field!(String);
impl_into_form_field!(i32, []);
impl_into_form_field!(Password);
#[cfg(feature = "secure-password")]
impl_into_form_field!(super::secure_password::BcryptPassword);
