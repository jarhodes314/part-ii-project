use bcrypt::{hash, verify, BcryptResult};
use rocket::{http::RawStr, request::FromFormValue};

pub struct BcryptPassword(String);

impl BcryptPassword {
    pub fn verify(&self, hash: &str) -> BcryptResult<bool> {
        verify(&self.0, hash)
    }

    pub fn hash(&self, cost: u32) -> BcryptResult<String> {
        hash(&self.0, cost)
    }
}

impl<'v> FromFormValue<'v> for BcryptPassword {
    type Error = &'v RawStr;

    fn from_form_value(form_value: &'v RawStr) -> Result<Self, Self::Error> {
        <String>::from_form_value(form_value).map(BcryptPassword)
    }
}
