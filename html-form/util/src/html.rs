//! The basis for the HTML generation
//!
//! This stores content in (hopefully) sensible datastructures for generating valid HTML. It aims
//! to make things such as whether a tag can store content type-safe.
use indexmap::IndexMap;
use itertools::Itertools;
use rocket_contrib::templates::tera;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, LinkedList};
use std::iter::FromIterator;

pub mod tags;

/// HTML internal representation, basically a list of tags. It is a list because HTML doesn't
/// necessarily have a root node, unlike XML.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Default)]
pub struct Html(pub LinkedList<Content>);

/// The things that can be content of an HTML tag
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum Content {
    /// Raw content, e.g. "Hello, world!" in `<p>Hello, world!</p>`
    Raw(String),
    /// A tag
    Tag(TagVariety),
}

/// An HTML tag
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum TagVariety {
    /// A tag which can have content, e.g. `<p>`, `<ul>`, `<label>`...
    Nest(Tag<Option<Html>>),
    /// A tag which shouldn't have content, e.g. `<input>`
    NonNest(Tag<()>),
}

/// An internal tag representation, designed to be used with `TagVariety`
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct Tag<T: Clone + Eq> {
    /// The tag name, e.g. label, input, a...
    pub name: String,
    /// The tag attributes, e.g. type="text", name="first-name"
    pub attributes: IndexMap<String, String>,
    /// The content which is nested, parameterised to allow prevention of nesting
    pub nested: T,
}

/// Macro to produce a getter for a common attribute in tags
macro_rules! getter {
    ($name: ident, $ty: ty) => {
        fn $name(&self) -> &$ty {
            match self {
                TagVariety::Nest(ref tag) => &tag.$name,
                TagVariety::NonNest(ref tag) => &tag.$name,
            }
        }
    };

    ($name: ident, $name_mut: ident, $ty: ty) => {
        getter!($name, $ty);

        fn $name_mut(&mut self) -> &mut $ty {
            match self {
                TagVariety::Nest(ref mut tag) => &mut tag.$name,
                TagVariety::NonNest(ref mut tag) => &mut tag.$name,
            }
        }
    };
}

impl TagVariety {
    getter!(name, String);
    getter!(attributes, attributes_mut, IndexMap<String, String>);
}

/// A tag construction macro, usage e.g. `tag!(Nest, "div", [ "id" => "a-div", "class" =>
/// "blue-border" ], Some(nested))`
#[macro_export]
macro_rules! tag {
    ($nest: ident, $name: expr, [ $($attr_name: expr => $attr_value: expr ),* ], $nested: expr ) => {
         $crate::Content::Tag($crate::TagVariety::$nest($crate::Tag {
             name: String::from($name),
             attributes: indexmap::indexmap! {
                $(String::from($attr_name) => String::from($attr_value)),*
             },
             nested: $nested,
         }))
    };

    (NonNest, $name: expr, $tokens:tt) => {
        tag!(NonNest, $name, $tokens, ())
    };

    (Nest, $name: expr, $tokens:tt) => {
        tag!(Nest, $name, $tokens, None)
    };
}

/// A [tera] filter to output the HTML
pub fn form_filter<S: std::hash::BuildHasher>(
    value: &tera::Value,
    _args: &HashMap<String, tera::Value, S>,
) -> tera::Result<tera::Value> {
    fn generate_output(html: Html) -> tera::Result<tera::Value> {
        let output = html.iter().map(ToString::to_string).join("");

        Ok(tera::Value::String(output))
    }

    use crate::forms::{FieldType, SerializeField, SerializeForm};

    if let Ok(html) = tera::from_value::<Html>(value.to_owned()) {
        generate_output(html)
    } else if let Ok(form) = tera::from_value::<SerializeForm>(value.to_owned()) {
        generate_output(form.html())
    } else if let Ok(field) = tera::from_value::<SerializeField>(value.to_owned()) {
        generate_output(field.html())
    } else if let Ok(field) = tera::from_value::<FieldType>(value.to_owned()) {
        generate_output(field.html())
    } else {
        let error = "This can only be applied to HTML".into();
        Err(error)
    }
}

impl ToString for Html {
    fn to_string(&self) -> String {
        self.iter()
            .fold(String::new(), |acc, content| acc + &content.to_string())
    }
}

impl ToString for Content {
    fn to_string(&self) -> String {
        use Content::*;

        match self {
            Raw(content) => content.to_string(),
            Tag(tag) => tag.to_string(),
        }
    }
}

impl ToString for TagVariety {
    fn to_string(&self) -> String {
        let name = self.name();
        let attributes = self
            .attributes()
            .iter()
            .map(|(k, v)| format!(r#"{}="{}""#, k, v))
            .join(" ");

        match self {
            TagVariety::Nest(tag) => {
                let content = tag
                    .nested
                    .as_ref()
                    .map(|tag| tag.iter().map(ToString::to_string).join(""))
                    .unwrap_or_default();
                format!("<{name} {}>{}</{name}>", attributes, content, name = name)
            }
            TagVariety::NonNest(_) => format!("<{} {}>", name, attributes),
        }
    }
}

impl<'a> Html {
    /// Create an Html object
    pub fn new() -> Html {
        Html::default()
    }

    /// Appends one HTML tree to another, at the top level
    pub fn append(&mut self, html: &mut Html) {
        self.0.append(&mut html.0);
    }

    /// Appends one node to the HTML tree, at the top level
    pub fn push_back(&mut self, html: Content) {
        self.0.push_back(html);
    }

    /// Creates an iterator for the HTML tree
    pub fn iter(&'a self) -> std::collections::linked_list::Iter<'a, Content> {
        self.0.iter()
    }

    fn todo() {} // TODO make below not fail silently
    /// Appends one HTML tree to another, by nesting inside the tag if possible
    ///
    /// This will fail silently if the content in the tree is not nestable
    pub fn nest_back(&mut self, mut nest: Html) {
        let html = self.0.front_mut();

        if let Some(Content::Tag(TagVariety::Nest(ref mut tag))) = html {
            if let Some(ref mut nested) = tag.nested.as_mut() {
                nested.append(&mut nest);
            } else {
                tag.nested = Some(nest);
            }
        } else {
            self.append(&mut nest);
        }
    }

    /// Sets an attribute on the first element at the top level of an HTML tree
    pub fn set_attribute(&mut self, name: &str, value: &str) {
        use Content::Tag;

        if let Some(Tag(ref mut tag)) = self.0.front_mut() {
            tag.attributes_mut()
                .insert(name.to_string(), value.to_string());
        }
    }

    /// Sets a number of attributes on the first element at the top level of an HTML tree
    pub fn set_attributes(&mut self, attributes: &IndexMap<String, String>) {
        use Content::Tag;

        if let Some(Tag(ref mut tag)) = self.0.front_mut() {
            for (name, value) in attributes {
                tag.attributes_mut()
                    .insert(name.to_string(), value.to_string());
            }
        }
    }

    /// Sets a number of attributes on the first element at the top level of an HTML tree
    pub fn set_attributes_vec(&mut self, attributes: &[(&'static str, String)]) {
        use Content::Tag;

        if let Some(Tag(ref mut tag)) = self.0.front_mut() {
            for (name, value) in attributes {
                tag.attributes_mut()
                    .insert((*name).to_string(), value.to_string());
            }
        }
    }

    /// Sets an attribute optionally, based on whether it has a value
    pub fn set_option_attribute(&mut self, name: &str, value: Option<&str>) {
        if let Some(value) = value {
            self.set_attribute(name, value);
        }
    }

    /// Creates a tree with a single element of Raw content
    pub fn raw_text(content: String) -> Html {
        let mut html = Html::new();
        html.push_back(Content::Raw(content));
        html
    }

    /// Creates a nestable tag with the given name
    pub fn nestable_tag(name: &str) -> Content {
        Content::Tag(TagVariety::Nest(Tag {
            name: name.to_owned(),
            attributes: IndexMap::new(),
            nested: None,
        }))
    }
}

impl<'a> IntoIterator for &'a Html {
    type Item = &'a Content;
    type IntoIter = std::collections::linked_list::Iter<'a, Content>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl FromIterator<Html> for Html {
    fn from_iter<I: IntoIterator<Item = Html>>(iter: I) -> Self {
        let mut html = Self::new();

        for mut i in iter {
            html.0.append(&mut i.0)
        }

        html
    }
}
