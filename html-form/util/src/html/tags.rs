//! Tag implementation for input
use super::{Content, Tag, TagVariety};
use indexmap::IndexMap;

/// A tag that can be turned into [Content]
pub trait HtmlTag {
    /// Turn the tag into html with the given attributes and starting value (intended for inputs)
    fn html(attributes: &IndexMap<String, String>, value: &Option<String>) -> Content;
}

/// An `<input>` tag
///
/// The type is set by attributes on the [html](HtmlTag::html) method
pub struct Input;

impl HtmlTag for Input {
    fn html(attributes: &IndexMap<String, String>, value: &Option<String>) -> Content {
        let mut attributes = attributes.to_owned();
        value
            .as_ref()
            .map(|value| attributes.insert(String::from("value"), value.to_owned()));
        Content::Tag(TagVariety::NonNest(Tag {
            name: String::from("input"),
            attributes,
            nested: (),
        }))
    }
}
