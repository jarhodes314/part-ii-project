//! # HTML Form util crate
//!
//! This is baically rexported by the top level crate. It is used to provide trait definitions, the
//! form and html structs.
//#![cfg_attr(feature = "strict", deny(missing_docs))]
//#![deny(clippy::all)]
#![warn(
    clippy::all,
    clippy::restriction,
    clippy::pedantic,
    clippy::nursery,
    //clippy::cargo,
)]
#![allow(
    clippy::restriction::implicit_return,
    clippy::restriction::missing_inline_in_public_items
)]
#![feature(const_fn)]
#![feature(associated_type_defaults)]
use proc_macro2::TokenStream;
use quote::{quote, quote_spanned, ToTokens};
use syn::{
    parenthesized,
    parse::{Parse, ParseStream, Result as ParseResult},
    spanned::Spanned,
    Ident, Token,
};

mod html;
pub use html::*;
use rocket::request::FromForm;

pub mod forms;

pub mod select;
pub mod validation;

pub trait HtmlForm<'f>: FromForm<'f> + validation::BackendValidated {
    fn generate_form() -> forms::Form<'f>;
}

#[derive(Clone, Debug, Default)]
pub struct HtmlAttributes {
    pub label: Option<String>,
    pub value: Option<String>,
    pub placeholder: Option<String>,
}

impl HtmlAttributes {
    pub fn new() -> HtmlAttributes {
        Default::default()
    }

    pub fn reduce(
        acc: ParseResult<HtmlAttributes>,
        attrs: ParseResult<HtmlAttributes>,
    ) -> ParseResult<HtmlAttributes> {
        acc.and_then(|acc| {
            let attrs = attrs?;
            Ok(HtmlAttributes {
                label: acc.label.or(attrs.label),
                value: acc.value.or(attrs.value),
                placeholder: acc.placeholder.or(attrs.placeholder),
            })
        })
    }
}

impl Parse for HtmlAttributes {
    fn parse(input: ParseStream) -> ParseResult<Self> {
        let mut result = Self::new();
        let content;

        parenthesized!(content in input);
        let values_result =
            content.parse_terminated::<IdentStringPair, Token![,]>(IdentStringPair::parse);
        let values = values_result?;

        for argument in values {
            match argument.name.to_string().as_str() {
                "label" => result.label = Some(argument.value),
                "value" => result.value = Some(argument.value),
                "placeholder" => result.placeholder = Some(argument.value),
                _ => {
                    return Err(syn::Error::new(
                        argument.name.span(),
                        "Cannot parse argument",
                    ))
                }
            };
        }

        Ok(result)
    }
}

macro_rules! quote_option {
    ($var: ident) => {
        match $var {
            None => quote! { None },
            Some(v) => {
                let span = v.span();
                quote_spanned! { span => Some(#v.into()) }
            }
        }
    };
}

impl ToTokens for HtmlAttributes {
    fn to_tokens(&self, ts: &mut TokenStream) {
        let HtmlAttributes {
            label,
            value,
            placeholder,
        } = self;
        let label = quote_option!(label);
        let value = quote_option!(value);
        let placeholder = quote_option!(placeholder);
        let tokens = quote! {HtmlAttributes {
            label: #label,
            value: #value,
            placeholder: #placeholder,
        }};
        tokens.to_tokens(ts);
    }
}

#[derive(Debug)]
pub struct IdentStringPair {
    pub name: Ident,
    pub eq_token: Token![=],
    pub value: String,
}

impl Parse for IdentStringPair {
    fn parse(input: ParseStream) -> ParseResult<Self> {
        Ok(Self {
            name: input.parse()?,
            eq_token: input.parse()?,
            value: input.parse::<syn::LitStr>()?.value(),
        })
    }
}
