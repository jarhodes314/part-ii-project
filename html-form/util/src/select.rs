#![allow(clippy::pedantic::module_name_repetitions)]
pub trait SelectOne: Sized {
    type UniqueSelector;
    fn unique_selector() -> Self::UniqueSelector;
    fn value(selector: Self::UniqueSelector) -> Self;
}

pub trait SelectMany: Sized {
    type MultipleSelector;
    fn multiple_selector() -> Self::MultipleSelector;
    fn values(selector: Self::MultipleSelector) -> Vec<Self>;
}
