use itertools::Itertools;
use std::ops::{
    Range, RangeBounds, RangeFrom, RangeFull, RangeInclusive, RangeTo, RangeToInclusive,
};

pub trait FrontendValidator: std::fmt::Debug {
    /// The JavaScript for the element's validation
    fn js(&self) -> String;
    /// The extra attributes required for HTML5 validation
    fn validation_attributes(&self) -> Vec<(&'static str, String)>;
}

pub trait FrontendValidated: std::fmt::Debug {
    fn validators(&self) -> Vec<String>;
    /// The extra attributes required for HTML5 validation
    fn validation_attributes(&self) -> Vec<(&'static str, String)>;
}

pub trait BackendValidated {
    fn validate_form_values(&self) -> Result<(), Vec<Box<dyn std::fmt::Debug>>>;
}

pub trait ValidationRule<T>: std::fmt::Debug + FrontendValidator {
    fn validate(&self, value: &T) -> Result<(), Box<dyn std::fmt::Debug>>;
}

#[derive(Debug)]
pub struct DoubleEntry {
    /// The ident of the original field, ie not the confirmation
    pub ident: &'static str,
    pub label: Option<&'static str>,
}

impl FrontendValidator for DoubleEntry {
    fn js(&self) -> String {
        format!(
            "selector(fieldName) === {value_original} ? [true] : [false, \"{ident}s should match\"]",
            value_original = format!(r#"selector("{}")"#, self.ident),
            ident = self.label.unwrap_or(self.ident),
        )
    }

    fn validation_attributes(&self) -> Vec<(&'static str, String)> {
        vec![]
    }
}

impl<E: Eq> ValidationRule<E> for DoubleEntry {
    fn validate(&self, _: &E) -> Result<(), Box<dyn std::fmt::Debug>> {
        Ok(())
    }
}

#[derive(Debug)]
pub struct MinimumLength(pub usize);

#[derive(Debug)]
pub struct NotLongEnough {
    length: usize,
    required_length: usize,
}

impl FrontendValidator for MinimumLength {
    fn js(&self) -> String {
        format!(
            "selector(fieldName).length >= {n} ? [true] : [false, \"value should be at least {n} characters\"]",
            n = self.0
        )
    }

    fn validation_attributes(&self) -> Vec<(&'static str, String)> {
        vec![("minlength", self.0.to_string())]
    }
}

impl ValidationRule<String> for MinimumLength {
    fn validate(&self, value: &String) -> Result<(), Box<dyn std::fmt::Debug>> {
        let length = value.chars().count();

        if length >= self.0 {
            Ok(())
        } else {
            Err(Box::new(NotLongEnough {
                length,
                required_length: self.0,
            }))
        }
    }
}

#[derive(Debug)]
pub struct MaximumSymbols(pub usize);

impl FrontendValidator for MaximumSymbols {
    fn js(&self) -> String {
        format!(
            r#"selector(fieldName).match(/[@#$%^&*()_+\-=[]{{}};':"|,.<>/?]/g).length <= {n} ? [true] : [false, \"value should have at most {n} special characters\"]"#,
            n = self.0
        )
    }

    fn validation_attributes(&self) -> Vec<(&'static str, String)> {
        vec![]
    }
}

#[derive(Debug)]
pub struct TooManySymbols {
    count: usize,
    maximum_allowed: usize,
}

impl ValidationRule<String> for MaximumSymbols {
    fn validate(&self, value: &String) -> Result<(), Box<dyn std::fmt::Debug>> {
        let count = value.chars().filter(|c| !c.is_alphanumeric()).count();

        if count <= self.0 {
            Ok(())
        } else {
            Err(Box::new(TooManySymbols {
                count,
                maximum_allowed: self.0,
            }))
        }
    }
}

pub enum RangeType<T> {
    Range(Range<T>),
    RangeFrom(RangeFrom<T>),
    RangeFull(RangeFull),
    RangeInclusive(RangeInclusive<T>),
    RangeTo(RangeTo<T>),
    RangeToInclusive(RangeToInclusive<T>),
}

#[macro_export]
macro_rules! InRange {
    [$start:tt .. $end: tt]       => { $crate::validation::RangeType::Range($start..$end) };
    [$start:tt ..]                => { $crate::validation::RangeType::RangeFrom($start..) };
    [..]                          => { $crate::validation::RangeType::RangeFull(..) };
    [$start:tt ..= $end: tt]      => { $crate::validation::RangeType::RangeInclusive($start..=$end) };
    [.. $end: tt]                 => { $crate::validation::RangeType::RangeTo(..$end) };
    [..= $end: tt]                => { $crate::validation::RangeType::RangeToInclusive($start..=$end) };
}

impl<T: std::fmt::Debug> std::fmt::Debug for RangeType<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use RangeType::*;
        match self {
            Range(range) => range.fmt(f),
            RangeFrom(range) => range.fmt(f),
            RangeFull(range) => range.fmt(f),
            RangeInclusive(range) => range.fmt(f),
            RangeTo(range) => range.fmt(f),
            RangeToInclusive(range) => range.fmt(f),
        }
    }
}

impl<T> std::ops::RangeBounds<T> for RangeType<T> {
    fn start_bound(&self) -> std::ops::Bound<&T> {
        use RangeType::*;
        match self {
            Range(range) => range.start_bound(),
            RangeFrom(range) => range.start_bound(),
            RangeFull(range) => range.start_bound(),
            RangeInclusive(range) => range.start_bound(),
            RangeTo(range) => range.start_bound(),
            RangeToInclusive(range) => range.start_bound(),
        }
    }

    fn end_bound(&self) -> std::ops::Bound<&T> {
        use RangeType::*;
        match self {
            Range(range) => range.end_bound(),
            RangeFrom(range) => range.end_bound(),
            RangeFull(range) => range.end_bound(),
            RangeInclusive(range) => range.end_bound(),
            RangeTo(range) => range.end_bound(),
            RangeToInclusive(range) => range.end_bound(),
        }
    }
}

pub trait Increment: Sized {
    fn increment(&self) -> Self;
}

pub trait Decrement: Sized {
    fn decrement(&self) -> Self;
}

#[allow(clippy::restriction::as_conversions)]
macro_rules! impl_incr_decr {
    ($ty: ty) => {
        impl Increment for $ty {
            fn increment(&self) -> Self {
                self + (1 as $ty)
            }
        }

        impl Decrement for $ty {
            fn decrement(&self) -> Self {
                self - (1 as $ty)
            }
        }
    };
}

impl_incr_decr!(i32);

impl<T> FrontendValidator for RangeType<T>
where
    T: std::fmt::Debug + PartialOrd<T> + Increment + Decrement + ToString,
{
    fn js(&self) -> String {
        use RangeType::*;

        macro_rules! lower_bound {
            () => {
                "selector(fieldName) >= {start:?}"
            };
        };
        macro_rules! upper_bound {
            () => {
                "selector(fieldName) < {end:?}"
            };
        };
        macro_rules! upper_bound_inc {
            () => {
                "selector(fieldName) <= {end:?}"
            };
        };

        let bounds = match self {
            Range(range) => vec![
                format!(lower_bound!(), start = range.start),
                format!(upper_bound!(), end = range.end),
            ],
            RangeFrom(range) => vec![format!(lower_bound!(), start = range.start)],
            RangeFull(_) => vec![],
            RangeInclusive(range) => vec![
                format!(lower_bound!(), start = range.start()),
                format!(upper_bound_inc!(), end = range.end()),
            ],
            RangeTo(range) => vec![format!(upper_bound!(), end = range.end)],
            RangeToInclusive(range) => vec![format!(upper_bound_inc!(), end = range.end)],
        };

        format!(
            "{} ?
            [true] :
            [false, `${{selector(fieldName)}} not in correct range {:?}`]",
            bounds.iter().join(" & "),
            self,
        )
    }

    fn validation_attributes(&self) -> Vec<(&'static str, String)> {
        use std::ops::Bound::*;
        let mut result = Vec::new();

        match self.start_bound() {
            Included(lower) => result.push(("min", lower.to_string())),
            Excluded(lower) => result.push(("min", lower.increment().to_string())),
            _ => (),
        };

        match self.end_bound() {
            Included(upper) => result.push(("max", upper.to_string())),
            Excluded(upper) => result.push(("max", upper.decrement().to_string())),
            _ => (),
        };

        result
    }
}

impl<T: std::fmt::Debug + PartialOrd<T> + Increment + Decrement + ToString> ValidationRule<T>
    for RangeType<T>
{
    fn validate(&self, value: &T) -> Result<(), Box<dyn std::fmt::Debug>> {
        if self.contains(value) {
            Ok(())
        } else {
            Err(Box::new(format!("{:?} not in range {:?}", value, self))
                as Box<dyn std::fmt::Debug>)
        }
    }
}
