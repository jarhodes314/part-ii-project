//! # Examples of codegen prep
//!
//! This crate (will) contain examples of how the demonstration macros I've created work. It should
//! hopefully provide some background to the `project-prep-codegen` crate's contents, which would
//! otherwise be a bit mysterious, probably.
// TODO remove the "will" when that no longer applies

#[macro_use] extern crate project_prep_codegen;

#[derive(FussyStrings, CustomDebug)]
struct Hello {
    world: String,
    test: i32,
}

#[derive(Debug)]
enum Hello2 {
    f1{ number: i32},
    f2(f64),
}

#[derive(CustomDebug)]
enum MadEnum {
    Something,
    SomethingElse(String, i32),
    AnotherThing{thing: String, thingy: f64},
}
