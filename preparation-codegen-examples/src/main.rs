#[macro_use] extern crate project_prep_codegen;

#[derive(Debug)]
struct DebugDemo {
    name: &'static str,
    age: usize,
}

//#[derive(CustomDebug)]
//enum

fn main() {
    println!("Custom debug demo");

    println!("{:?}", DebugDemo { name: "A Demo", age: 38 });
}
