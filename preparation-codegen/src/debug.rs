//! The custom debug implementation
//!
//! This is an example of how to write a custom debug, based on manual implementation example at
//! https://doc.rust-lang.org/std/fmt/trait.Debug.html
use quote::ToTokens;
use syn::{spanned::Spanned, Data, DataEnum, DataStruct, DeriveInput, Field, Fields, Ident, Index};

pub fn custom_debug_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let input_ident = ast.ident;

    match ast.data {
        Data::Struct(ref s) => custom_debug_struct(input_ident, s),
        Data::Enum(ref e) => custom_debug_enum(input_ident, e),
        Data::Union(_) => abort_call_site!("this trait cannot be derived for unions"),
    }
    .into()
}

/// An enum for storing either the identifier or index of a field
#[derive(Debug)]
enum FieldIdent {
    Named(Ident),
    Unnamed(Index),
}

impl ToTokens for FieldIdent {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        use FieldIdent::*;

        match self {
            Named(ident) => ident.to_tokens(tokens),
            Unnamed(index) => index.to_tokens(tokens),
        };
    }
}

fn collect_fields(fields: &Fields) -> Vec<Field> {
    match fields {
        syn::Fields::Unit => Vec::new(),
        syn::Fields::Named(ref fields) => fields.named.clone().into_iter().collect(),
        syn::Fields::Unnamed(ref fields) => fields.unnamed.clone().into_iter().collect(),
    }
}

/// Custom debug implementation for structs
fn custom_debug_struct(struct_ident: Ident, struct_: &DataStruct) -> proc_macro2::TokenStream {
    let mut fields = collect_fields(&struct_.fields);
    let mut idents = Vec::with_capacity(fields.len());

    // Iterate over fields (for named fields) and indices (for anonymous fields)
    for (field, index) in fields.iter_mut().zip(0..) {
        let span = field.span();
        match &mut field.ident {
            Some(ref mut ident) => {
                ident.set_span(span);
                idents.push(FieldIdent::Named(ident.to_owned()));
            },
            None => idents.push(FieldIdent::Unnamed(Index {
                index,
                span,
            })),
        }
    }

    // Create a format string for the write! macro
    let field_name_values = fields
        .iter()
        .map(|field| {
            if let Some(ref ident) = field.ident {
                ident.to_string() + ": {:?}"
            } else {
                "{:?}".to_string()
            }
        })
        .collect::<Vec<String>>()
        .join(", ");

    let contents = match struct_.fields {
        syn::Fields::Unit => format!("{}", struct_ident),
        // Double escape the { and } at the start and end of the struct since they become part of a
        // new format string and want to be escaped there
        syn::Fields::Named(_) => format!("{} {{{{ {} }}}}", struct_ident, field_name_values),
        syn::Fields::Unnamed(_) => format!("{}({})", struct_ident, field_name_values),
    };

    let idents = idents
        .iter()
        .map(|ident| quote_spanned!(ident.span() => self.#ident));
    let write = quote! {
        write!(f, #contents, #(#idents),*)
    };

    quote! {
        impl core::fmt::Debug for #struct_ident {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                #write
            }
        }
    }
}

/// An enum for storing either the identifiers or indices of a field
enum FieldIdents {
    Unit,
    Named(Vec<Ident>),
    Unnamed(Vec<Ident>),
}

impl From<&Fields> for FieldIdents {
    /// Convert vec field into FieldIdents
    fn from(fields: &Fields) -> FieldIdents {
        match fields {
            syn::Fields::Unit => FieldIdents::Unit,
            syn::Fields::Named(ref fields) => FieldIdents::Named(
                fields
                    .named
                    .iter()
                    .map(|field| {
                        let mut ident = field.ident.clone().unwrap();
                        ident.set_span(field.span());
                        ident
                    })
                    .collect(),
            ),
            syn::Fields::Unnamed(ref fields) => FieldIdents::Unnamed(
                fields
                    .unnamed
                    .iter()
                    .zip(0..)
                    .map(|(field, index)| {
                        Ident::new(&format!("v{}", index), field.span())
                    })
                    .collect(),
            ),
        }
    }
}

/// Generate the match arm headings for an enum
fn enum_match_arm((variant_ident, idents): &(Ident, FieldIdents)) -> proc_macro2::TokenStream {
    use FieldIdents::*;

    match idents {
        Unit => quote! { #variant_ident },
        Named(ref idents) => {
            quote! { #variant_ident{ #(ref #idents),*} }
        }
        Unnamed(ref idents) => {
            quote! { #variant_ident(#(ref #idents),*) }
        }
    }
}

fn variant_format_string(variant_ident: &Ident, idents: &FieldIdents) -> String {
    match idents {
        FieldIdents::Unit => variant_ident.to_string(),
        FieldIdents::Named(ref idents) => {
            let ident_formats = idents
                .iter()
                // Create a name: value format string
                .map(|ident| format!("{}: {{:?}}", ident))
                .collect::<Vec<String>>();
            // Result is name { field1: value1, field2: value2, ... }
            format!("{} {{{{ {} }}}}", variant_ident, ident_formats.join(", "))
        }
        FieldIdents::Unnamed(ref indices) => {
            let index_formats = indices
                .iter()
                // Create a value format string
                .map(|_| "{:?}".into())
                .collect::<Vec<String>>();
            // Result is name(value1, value2, ...)
            format!("{}({})", variant_ident, index_formats.join(", "))
        }
    }
}

/// Custom debug implementation for enums
fn custom_debug_enum(enum_ident: Ident, enum_: &DataEnum) -> proc_macro2::TokenStream {
    let variants = &enum_.variants;
    let variants_fields = variants
        .iter()
        .map(|var| (var.ident.clone(), (&var.fields).into()))
        .collect::<Vec<(Ident, FieldIdents)>>();
    let match_arms = variants_fields.iter().map(enum_match_arm);

    // Create a format string for the write! macro, for each variant of the enum
    let format_strings = variants_fields
        .iter()
        .map(|(v_i, i)| variant_format_string(v_i, i));

    let write_args = variants_fields.iter().map(|(_, idents)| match idents {
        FieldIdents::Unit => quote! {},
        FieldIdents::Named(ref idents) | FieldIdents::Unnamed(ref idents) => quote! { #(#idents),*},
    });

    let match_ = quote! {
        match self {
            #(#enum_ident::#match_arms => write!(f, #format_strings, #write_args)),*
        }
    };

    quote! {
        impl core::fmt::Debug for #enum_ident {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                #match_
            }
        }
    }
}
