//! A simple procedural macro crate used to learn about procedural macros as part of my Part II
//! project
//!
//! This has some simple procedural macros that do useful stuff, e.g. a custom derive and parsing a
//! struct. It has some tests which use `compiletest` to check compiler output and errors.
#![feature(proc_macro_diagnostic)]
#[macro_use]
extern crate syn;
#[macro_use]
extern crate proc_macro_error;
#[macro_use]
extern crate quote;
extern crate proc_macro;

use syn::{spanned::Spanned, Data, DeriveInput, Ident};

mod debug;

/// A custom derive macro that emits a compiler error if the input is not a struct
#[proc_macro_derive(StructOnly)]
#[proc_macro_error]
pub fn struct_only_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);

    match ast.data {
        Data::Struct(_) => (),
        _ => {
            // Fail if not applied to struct
            abort_call_site!("This macro can only be applied to structs");
        }
    }

    let output = quote! {};

    proc_macro::TokenStream::from(output)
}

/// A test macro for compiler warnings — emit warning if a field has type `String`
#[proc_macro_derive(FussyStrings)]
pub fn fussy_strings_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);

    match ast.data {
        Data::Struct(ref s) => {
            for field in &s.fields {
                match field.ty {
                    syn::Type::Path(ref type_path) => {
                        if type_path
                            .path
                            .is_ident(&Ident::new("String", type_path.span()))
                        {
                            field
                                .span()
                                .unstable()
                                .warning("I don't like strings")
                                .emit();
                        }
                    }
                    _ => (),
                };
            }
        }
        Data::Enum(ref _e) => {}
        _ => (),
    };

    (quote! {}).into()
}

#[proc_macro_derive(CustomDebug)]
#[proc_macro_error]
pub fn custom_debug_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    debug::custom_debug_derive(input)
}
