#![allow(dead_code)]
#[macro_use]
extern crate project_prep_codegen;

struct Test;

#[derive(CustomDebug)]
enum AlsoFailsPrecondition {
    Something(Test), //~ ERROR `Test` doesn't implement `std::fmt::Debug`
    AnotherThing{
        stuff: String,
        age: usize,
        test: Test, //~ ERROR `Test` doesn't implement `std::fmt::Debug`
    }
}

fn main() {}
