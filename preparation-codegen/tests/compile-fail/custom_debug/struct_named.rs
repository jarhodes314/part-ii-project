#![allow(dead_code)]
#[macro_use]
extern crate project_prep_codegen;

struct Test;

#[derive(CustomDebug)]
struct FailsPrecondition {
    test: Test, //~ ERROR `Test` doesn't implement `std::fmt::Debug`
}

#[derive(Debug)]
struct FailsPreconditionExample {
    test: Test, //~ ERROR `Test` doesn't implement `std::fmt::Debug`
}

fn main() {}
