#![allow(dead_code)]
#[macro_use]
extern crate project_prep_codegen;

struct Test;

#[derive(CustomDebug)]
struct AnotherFailure(
    i32,
    Option<Test>, //~ ERROR `Test` doesn't implement `std::fmt::Debug`
    String,
);

#[derive(Debug)]
struct AnotherDemoFailure(
    i32,
    Option<Test>, //~ ERROR `Test` doesn't implement `std::fmt::Debug`
    String,
);

fn main() {}
