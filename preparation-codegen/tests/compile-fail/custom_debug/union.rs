#![allow(dead_code)]
#[macro_use]
extern crate project_prep_codegen;

#[derive(CustomDebug)] //~ ERROR this trait cannot be derived for unions
union CStuff {
    f1: i32,
    f2: f64,
}

fn main() {}
