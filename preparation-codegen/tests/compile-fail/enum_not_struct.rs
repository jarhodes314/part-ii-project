#[macro_use]
extern crate project_prep_codegen;

#[derive(StructOnly)] //~ ERROR This macro can only be applied to structs
enum MyOption<T> {
    Some(T),
    None,
}

fn main() {}
