#[macro_use]
extern crate project_prep_codegen;

#[derive(CustomDebug)]
enum MadEnum {
    Something,
    SomethingElse(String, i32),
    AnotherThing { thing: String, thingy: f64 },
}

mod normal_debug {
    #[derive(Debug)]
    pub enum MadEnum {
        Something,
        SomethingElse(String, i32),
        AnotherThing { thing: String, thingy: f64 },
    }
}

fn main() {
    let value = MadEnum::Something;
    let value_ = normal_debug::MadEnum::Something;
    assert_eq!(format!("{:?}", value), format!("{:?}", value_));

    let value = MadEnum::SomethingElse("Hello".into(), 183);
    let value_ = normal_debug::MadEnum::SomethingElse("Hello".into(), 183);
    assert_eq!(format!("{:?}", value), format!("{:?}", value_));

    let value = MadEnum::AnotherThing {
        thing: "Hello".into(),
        thingy: 2.178,
    };
    let value_ = normal_debug::MadEnum::AnotherThing {
        thing: "Hello".into(),
        thingy: 2.178,
    };
    assert_eq!(format!("{:?}", value), format!("{:?}", value_));
}
