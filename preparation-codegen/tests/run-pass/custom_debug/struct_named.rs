#[macro_use]
extern crate project_prep_codegen;

#[derive(CustomDebug)]
struct SomeStruct {
    name: &'static str,
    age: usize,
}
mod normal_debug {
    #[derive(Debug)]
    pub struct SomeStruct {
        pub name: &'static str,
        pub age: usize,
    }
}

fn main() {
    let value = SomeStruct {
        name: "Test",
        age: 7,
    };
    let value_ = normal_debug::SomeStruct {
        name: "Test",
        age: 7,
    };

println!("{:?}", value);

    assert_eq!(format!("{:?}", value), format!("{:?}", value_));
}
