#[macro_use]
extern crate project_prep_codegen;

#[derive(CustomDebug)]
struct Unit;

mod normal_debug {
    #[derive(Debug)]
    pub struct Unit;
}

fn main() {
    let value = Unit;
    let value_ = normal_debug::Unit;
    assert_eq!(format!("{:?}", value), format!("{:?}", value_));
}
