#[macro_use]
extern crate project_prep_codegen;

#[derive(CustomDebug)]
struct Anon(i32, f64);

mod normal_debug {
    #[derive(Debug)]
    pub struct Anon(pub i32, pub f64);
}

fn main() {
    let value = Anon(-38, 3.141);
    let value_ = normal_debug::Anon(-38, 3.141);
    assert_eq!(format!("{:?}", value), format!("{:?}", value_));
}
