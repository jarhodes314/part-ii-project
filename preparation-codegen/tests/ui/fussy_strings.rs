#![allow(dead_code)]
#[macro_use]
extern crate project_prep_codegen;

#[derive(FussyStrings)]
struct Fussy {
    hello: i32,
    world: String,
    //^ WARN I don't like strings
}

#[derive(FussyStrings)]
struct Unfussy {
    test: usize,
}

fn main() {}
