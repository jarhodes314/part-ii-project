// Enable some required crate features for Rocket
#![feature(decl_macro, proc_macro_hygiene)]

//! # Prep work site
//!
//! This is the website, created using Rocket, that I am using as a base to try things out, such as
//! hand derived `FromForm` traits (and maybe some declarative macro stuff).

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde;

use rocket::request::{Form, LenientForm};
use rocket::{Rocket, Route};
use rocket_contrib::json::Json;
mod manual_fromform;
#[cfg(test)]
mod tests;

fn routes() -> Vec<Route> {
    routes![hello, sign_up, user_receive, user_receive_lenient]
}

#[get("/hello/<name>")]
fn hello(name: String) -> String {
    format!("Hello, {}", name)
}

#[derive(Clone, FromForm, Serialize)]
struct NewUser {
    name: String,
    age: usize,
}

#[post("/sign-up", data = "<data>")]
fn sign_up(data: Form<NewUser>) -> Json<NewUser> {
    Json(data.into_inner().clone())
}

#[post("/manual_fromform", data = "<user>")]
fn user_receive(user: Form<manual_fromform::User>) -> Json<manual_fromform::User> {
    Json(user.into_inner().clone())
}

#[post("/manual_fromform2", data = "<user>")]
fn user_receive_lenient(user: LenientForm<manual_fromform::User>) -> Json<manual_fromform::User> {
    Json(user.into_inner().clone())
}

fn rocket() -> Rocket {
    rocket::ignite().mount("/", routes())
}

fn main() {
    rocket().launch();
}
