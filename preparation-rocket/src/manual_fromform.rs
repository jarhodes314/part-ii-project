use rocket::request::{FormItems, FromForm};

#[derive(Clone, Serialize)]
pub struct User {
    pub username: String,
    pub name: Option<String>,
    pub account_number: i64,
}

#[derive(Debug)]
pub enum UserParseError {
    AccountNumberParseFail(String),
    UnrecognizedField(String),
    DuplicateField(String),
    MissingField(String),
}

impl<'f> FromForm<'f> for User {
    type Error = UserParseError;

    fn from_form(items: &mut FormItems<'f>, strict: bool) -> Result<Self, Self::Error> {
        use UserParseError::*;

        let mut username: Option<String> = None;
        let mut name: Option<Option<String>> = Some(None);
        let mut account_number: Option<i64> = None;

        for item in items {
            match item.key.as_str() {
                "username" => {
                    if username.is_none() {
                        username = Some(item.value.url_decode().unwrap().to_string());
                        Ok(())
                    } else {
                        Err(DuplicateField("username field duplicated".into()))
                    }
                }
                "name" => {
                    if name.as_ref().unwrap().is_none() {
                        name = Some(Some(item.value.url_decode().unwrap().to_string()));
                        Ok(())
                    } else {
                        Err(DuplicateField("name field duplicated".into()))
                    }
                }
                "account_number" => {
                    if account_number.is_none() {
                        account_number =
                            item.value
                                .parse::<i64>()
                                .map(Some)
                                .or(Err(AccountNumberParseFail(
                                    "account_number couldn't be parsed to integer".into(),
                                )))?;
                        Ok(())
                    } else {
                        Err(DuplicateField("account_number field duplicated".into()))
                    }
                }
                key => {
                    if strict {
                        Err(UnrecognizedField(format!("{} field not recognized", key)))
                    } else {
                        Ok(())
                    }
                }
            }?;
        }

        name.and_then(|name| {
            account_number.and_then(|account_number| {
                username.map(|username| User {
                    name,
                    account_number,
                    username,
                })
            })
        })
        .ok_or(MissingField("Something's missing".into()))
    }
}
