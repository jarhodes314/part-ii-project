use super::rocket;
use rocket::http::{ContentType, Status};
use rocket::local::Client;

#[test]
fn form_data_posts_successfully() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::sign_up);
    let mut response = client
        .post(route.path())
        .body("name=Testy%20Test&age=23")
        .header(ContentType::Form)
        .dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(
        response.body_string(),
        Some("{\"name\":\"Testy Test\",\"age\":23}".into())
    );
}

#[test]
fn form_data_fails_non_usize_age() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::sign_up);
    let response = client
        .post(route.path())
        .body("name=Testy%20Test&age=-6")
        .header(ContentType::Form)
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn manual_fromform_deserializes_successfully() {
    use crate::manual_fromform::User;

    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::user_receive);
    let mut response = client
        .post(route.path())
        .body("username=testy&name=Testy%20Test&account_number=4532")
        .header(ContentType::Form)
        .dispatch();

    let expecting = User {
        username: "testy".into(),
        name: Some("Testy Test".into()),
        account_number: 4532,
    };

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(
        response.body_string(),
        Some(serde_json::to_string(&expecting).expect("JSON Failure"))
    );
}

#[test]
fn manual_fromform_has_optional_name() {
    use crate::manual_fromform::User;

    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::user_receive);
    let mut response = client
        .post(route.path())
        .body("username=testy&account_number=4532")
        .header(ContentType::Form)
        .dispatch();

    let expecting = User {
        username: "testy".into(),
        name: None,
        account_number: 4532,
    };

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(
        response.body_string(),
        Some(serde_json::to_string(&expecting).expect("JSON Failure"))
    );
}

#[test]
fn manual_fromform_rejects_missing_username() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::user_receive);
    let response = client
        .post(route.path())
        .body("name=testy&account_number=4532")
        .header(ContentType::Form)
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn manual_fromform_rejects_extra_field_if_not_lenient() {
    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::user_receive);
    let response = client
        .post(route.path())
        .body("username=testy&name=Testy%20Test&account_number=4532&hash_browns=mmm")
        .header(ContentType::Form)
        .dispatch();

    assert_eq!(response.status(), Status::UnprocessableEntity);
}

#[test]
fn manual_fromform_ignores_extra_field_when_lenient() {
    use crate::manual_fromform::User;

    let client = Client::new(rocket()).expect("Couldn't create client");
    let route = uri!(super::user_receive_lenient);
    let mut response = client
        .post(route.path())
        .body("username=testy&name=Testy%20Test&account_number=4532&upton_upon_severn=waltz%3F%3F")
        .header(ContentType::Form)
        .dispatch();

    let expecting = User {
        username: "testy".into(),
        name: Some("Testy Test".into()),
        account_number: 4532,
    };

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(
        response.body_string(),
        Some(serde_json::to_string(&expecting).expect("JSON Failure"))
    );
}
